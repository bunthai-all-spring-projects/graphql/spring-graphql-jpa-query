package com.example.springgraphqljpaquery.configurations;


import com.example.springgraphqljpaquery.domain.Address;
import com.example.springgraphqljpaquery.domain.AddressCoercing;
import com.example.springgraphqljpaquery.domain.Author;
import com.introproventures.graphql.jpa.query.autoconfigure.GraphQLJpaQueryProperties;
import com.introproventures.graphql.jpa.query.autoconfigure.GraphQLSchemaConfigurer;
import com.introproventures.graphql.jpa.query.autoconfigure.GraphQLShemaRegistration;
import com.introproventures.graphql.jpa.query.schema.GraphQLExecutor;
import com.introproventures.graphql.jpa.query.schema.GraphQLSchemaBuilder;
import com.introproventures.graphql.jpa.query.schema.JavaScalars;
import com.introproventures.graphql.jpa.query.schema.impl.GraphQLJpaExecutor;
import com.introproventures.graphql.jpa.query.schema.impl.GraphQLJpaSchemaBuilder;
import graphql.Scalars;
import graphql.schema.GraphQLScalarType;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.dialect.H2Dialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.support.SharedEntityManagerBean;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class AuthorSchemaConfiguration {


    @Configuration
    public static class GraphQLJpaQuerySchemaConfigurer implements GraphQLSchemaConfigurer {

        @PersistenceContext
        private EntityManager entityManager;

        @Autowired
        GraphQLSchemaBuilder graphQLSchemaBuilder;

//        @Autowired
//        GraphQLExecutor graphQLExecutor;


        @Autowired
        private GraphQLJpaQueryProperties properties;

        @Override
        public void configure(GraphQLShemaRegistration registry) {
            registry.register(graphQLSchemaBuilder.build());
        }
    }

    @Bean
    public GraphQLExecutor graphQLExecutor(final GraphQLSchemaBuilder graphQLSchemaBuilder) {
        return new GraphQLJpaExecutor(graphQLSchemaBuilder.build());
    }

    @Bean
    public GraphQLSchemaBuilder graphQLSchemaBuilder(final EntityManager entityManager) {

        JavaScalars.register(Address.class, GraphQLScalarType.newScalar().name("Address").description("address our").coercing(new AddressCoercing()).build());

        return new GraphQLJpaSchemaBuilder(entityManager)
            .name("GraphQLAuthors")
            .description("Authors JPA test schema");
    }

}