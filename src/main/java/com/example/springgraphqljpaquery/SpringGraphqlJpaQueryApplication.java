package com.example.springgraphqljpaquery;

import com.example.springgraphqljpaquery.domain.Address;
import com.example.springgraphqljpaquery.domain.Author;
import com.example.springgraphqljpaquery.domain.Zip;
import com.example.springgraphqljpaquery.repo.AuthorRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.context.annotation.RequestScope;

import static graphql.schema.visibility.DefaultGraphqlFieldVisibility.DEFAULT_FIELD_VISIBILITY;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Supplier;

import javax.servlet.http.HttpServletRequest;

import graphql.GraphQLContext;
import graphql.execution.instrumentation.Instrumentation;
import graphql.execution.instrumentation.SimpleInstrumentation;
import graphql.execution.instrumentation.tracing.TracingInstrumentation;
import graphql.schema.visibility.BlockedFields;
import graphql.schema.visibility.GraphqlFieldVisibility;

@SpringBootApplication
@EnableTransactionManagement
public class SpringGraphqlJpaQueryApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SpringGraphqlJpaQueryApplication.class, args);
	}

	@Bean
	@RequestScope
	public Supplier<GraphqlFieldVisibility> graphqlFieldVisibility(HttpServletRequest request) {
		return () -> !request.isSecure() ? BlockedFields.newBlock()
			.addPattern("Author.name")
			.build()
			: DEFAULT_FIELD_VISIBILITY;
	}

	@Bean
	@RequestScope
	public Supplier<GraphQLContext> graphqlContext(HttpServletRequest request) {
		return () -> GraphQLContext.newContext()
			.of("request", request)
			.of("user", request)
			.build();
	}

	@Bean
	@RequestScope
	public Supplier<Instrumentation> instrumentation(HttpServletRequest request) {
		return () -> false
			? new TracingInstrumentation()
			: SimpleInstrumentation.INSTANCE;
	}


	@Autowired
	AuthorRepo authorRepo;
	@Override
	public void run(String... args) throws Exception {

		authorRepo.saveAll(
				List.of(
						Author.builder()
								.name("John")
								.birthday(LocalDate.now())
								.createdAt(LocalDateTime.now())
								.updatedAt(LocalDateTime.now())
								.address(Address.builder()
										.no(100)
										.street("ace3")
//										.zip(Zip.builder().code(12345).build())
										.build())
								.build(),
						Author.builder()
								.name("Farby")
								.birthday(LocalDate.now())
								.createdAt(LocalDateTime.now())
								.updatedAt(LocalDateTime.now())
								.address(Address.builder()
										.no(200)
										.street("yawny")
//										.zip(Zip.builder().code(12346).build())
										.build())
								.build()
				)

		);

	}
}
