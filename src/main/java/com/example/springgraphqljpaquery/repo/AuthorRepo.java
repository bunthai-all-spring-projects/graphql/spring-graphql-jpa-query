package com.example.springgraphqljpaquery.repo;

import com.example.springgraphqljpaquery.domain.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepo extends JpaRepository<Author, Integer> {
}
