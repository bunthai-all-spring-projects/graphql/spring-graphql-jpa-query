package com.example.springgraphqljpaquery.domain;

import graphql.schema.Coercing;
import graphql.schema.CoercingParseLiteralException;
import graphql.schema.CoercingParseValueException;
import graphql.schema.CoercingSerializeException;

public class AddressCoercing implements Coercing<Object, Object> {
    @Override
    public Object serialize(Object dataFetcherResult) throws CoercingSerializeException {
        System.out.println("Serialize" + dataFetcherResult);
        return dataFetcherResult;
    }

    @Override
    public Object parseValue(Object input) throws CoercingParseValueException {
        System.out.println("parseValue: " + input);
        return input;
    }

    @Override
    public Object parseLiteral(Object input) throws CoercingParseLiteralException {
        System.out.println("parseLiteral: " + input);
        return input;
    }
}
