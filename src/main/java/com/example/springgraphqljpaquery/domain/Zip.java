package com.example.springgraphqljpaquery.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Data
@NoArgsConstructor
@Embeddable
@Builder
@AllArgsConstructor
public class Zip {
    int code;
}
