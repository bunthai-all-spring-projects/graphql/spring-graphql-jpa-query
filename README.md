resource: https://github.com/introproventures/graphql-jpa-query/tree/0.5.x

URL: http://localhost:8080/graphql

request: 
```graphql
    query {
        Authors(where: {}, page: {start: 1, limit: 10}) {
            select {
                id
                name
            }
        }
    }

```


```curl
curl --request POST \
  --url http://localhost:8080/graphql \
  --header 'Content-Type: application/json' \
  --data '{"query":"query {\n\tAuthors(where: {}, page: {start: 1, limit: 10}) {\n\t\tselect {\n\t\t\tid\n\t\t\tname\n\t\t}\n\t}\n}\n"}'
```